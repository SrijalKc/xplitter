package com.example.x_splitter;


public class ModelMedia {
    private String media;

    public ModelMedia(String media) {
        this.media = media;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }
}
