package com.example.x_splitter;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
